# Requirements

Java 8+

# Build

`./mvnw clean package` (or `mvnw.cmd clean install` on Windows)

# Run

`java -jar target/PriceBasket.jar Apples`

