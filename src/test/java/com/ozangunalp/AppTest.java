package com.ozangunalp;

import static java.time.Month.NOVEMBER;
import static java.time.Month.OCTOBER;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.*;

import org.junit.Test;

public class AppTest {

    App app = new App(App.PRODUCT_CATALOG, App.OFFERS);

    @Test
    public void apples_discount() {
        Basket cart = app.createCart("Apples", "Milk", "Bread");
        Checkout checkout = app.checkout(cart, Instant.now());
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0.10);
        assertThat(checkout.getAppliedOffers()).hasSize(1);
        assertThat(checkout.getSubTotal()).isEqualTo(3.1);
        assertThat(checkout.getTotal()).isEqualTo(3.0);
    }

    @Test
    public void multiple_apples_discount() {
        Basket cart = app.createCart("Apples", "Apples");
        Checkout checkout = app.checkout(cart, Instant.now());
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0.20);
        assertThat(checkout.getAppliedOffers()).hasSize(1);
        assertThat(checkout.getSubTotal()).isEqualTo(2.0);
        assertThat(checkout.getTotal()).isEqualTo(1.8);
    }

    @Test
    public void soups_discount() {
        Basket cart = app.createCart("Soup", "Soup", "Bread");
        Checkout checkout = app.checkout(cart, LocalDateTime.of(2020, NOVEMBER, 8, 10, 2).toInstant(UTC));
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0.40);
        assertThat(checkout.getAppliedOffers()).hasSize(1);
        assertThat(checkout.getSubTotal()).isEqualTo(2.10);
        assertThat(checkout.getTotal()).isEqualTo(1.70);
    }

    @Test
    public void no_offer() {
        Basket cart = app.createCart("Milk", "Soup", "Bread");
        Checkout checkout = app.checkout(cart, Instant.now());
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0);
        assertThat(checkout.getAppliedOffers()).isEmpty();
        assertThat(checkout.getSubTotal()).isEqualTo(2.75);
        assertThat(checkout.getTotal()).isEqualTo(2.75);
    }

    @Test
    public void no_offer_out_of_time() {
        Basket cart = app.createCart("Apples");
        Checkout checkout = app.checkout(cart, LocalDateTime.of(2020, OCTOBER, 8, 10, 2).toInstant(UTC));
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0);
        assertThat(checkout.getAppliedOffers()).hasSize(0);
        assertThat(checkout.getSubTotal()).isEqualTo(1.0);
        assertThat(checkout.getTotal()).isEqualTo(1.0);
    }

    @Test
    public void multiple_offers() {
        Basket cart = app.createCart("Apples", "Milk", "Soup", "Soup", "Bread");
        Checkout checkout = app.checkout(cart, LocalDateTime.of(2020, NOVEMBER, 8, 10, 2).toInstant(UTC));
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0.5);
        assertThat(checkout.getAppliedOffers()).hasSize(2);
        assertThat(checkout.getSubTotal()).isEqualTo(4.40);
        assertThat(checkout.getTotal()).isEqualTo(3.90);
    }

    @Test
    public void multiple_same_offer() {
        Basket cart = app.createCart("Soup", "Soup", "Bread", "Soup", "Soup", "Bread");
        Checkout checkout = app.checkout(cart, LocalDateTime.of(2020, NOVEMBER, 8, 10, 2).toInstant(UTC));
        System.out.println(checkout.render());
        assertThat(checkout.getDiscount()).isEqualTo(0.80);
        assertThat(checkout.getAppliedOffers()).hasSize(1);
        assertThat(checkout.getSubTotal()).isEqualTo(4.20);
        assertThat(checkout.getTotal()).isEqualTo(3.40);
    }
}