/*
 * Copyright (C) by Courtanet, All Rights Reserved.
 */
package com.ozangunalp;

public class AppliedOffer {

    private final Offer offer;
    private final double discount;

    public AppliedOffer(Offer offer, double discount) {
        this.offer = offer;
        this.discount = discount;
    }

    public Offer getOffer() {
        return offer;
    }

    public double getDiscount() {
        return discount;
    }
}
