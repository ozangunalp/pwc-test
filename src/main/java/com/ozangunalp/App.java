package com.ozangunalp;

import static java.time.Month.NOVEMBER;
import static java.time.ZoneOffset.UTC;

import java.io.IOException;
import java.time.*;
import java.util.*;
import java.util.logging.LogManager;

public class App {

    public static final ProductCatalog PRODUCT_CATALOG = new ProductCatalog(
            new Product("Soup", 0.65),
            new Product("Bread", 0.80),
            new Product("Milk", 1.30),
            new Product("Apples", 1.00)
    );

    public static final List<Offer> OFFERS = Arrays.asList(
            new Offer("Apples have 10% off their normal price this week", "Apples 10% off",
                    instant -> instant.isAfter(LocalDateTime.of(2020, NOVEMBER, 7, 0, 0).toInstant(UTC))
                            && instant.isBefore(LocalDateTime.of(2020, NOVEMBER, 15, 0, 0).toInstant(UTC)),
                    (basket, catalog) -> basket.getProductCount("Apples") * catalog.getProduct("Apples").getPrice() * 0.1),
            new Offer("Buy two tins of soup and get a loaf of bread for half price", "50% off of bread",
                    instant -> true,
                    (basket, catalog) -> {
                        int breadCount = basket.getProductCount("Bread");
                        if (breadCount > 0 && (basket.getProductCount("Soup") / breadCount) >= 2) {
                            return breadCount * catalog.getProduct("Bread").getPrice() * 0.5;
                        } else {
                            return 0.0;
                        }
                    }
            ));

    public static void main(String[] args) throws IOException {
        LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logging.properties"));
        App app = new App(PRODUCT_CATALOG, OFFERS);
        Basket cart = app.createCart(args);
        System.out.println(app.checkout(cart, Instant.now(Clock.systemUTC())).render());
    }

    private final ProductCatalog catalog;
    private final List<Offer> offers;

    public App(ProductCatalog catalog, List<Offer> offers) {
        this.catalog = catalog;
        this.offers = offers;
    }

    public Basket createCart(String... productNames) {
        return new Basket(Arrays.stream(productNames)
                .map(PRODUCT_CATALOG::getProduct)
                .filter(Objects::nonNull)
                .map(Product::getName)
                .toArray(String[]::new));
    }

    public Checkout checkout(Basket basket, Instant instant) {
        return Checkout.checkout(instant, basket, catalog, offers);
    }
}
