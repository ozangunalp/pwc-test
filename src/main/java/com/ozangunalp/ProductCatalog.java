/*
 * Copyright (C) by Courtanet, All Rights Reserved.
 */
package com.ozangunalp;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Indexed list of products by name
 */
public class ProductCatalog {

    private final Map<String, Product> productMap;

    public ProductCatalog(Product... products) {
        this.productMap = Arrays.stream(products).collect(Collectors.toMap(Product::getName, p -> p));
    }

    /**
     * Find product by id
     * @param productName product name
     * @return Product if exists, null if not
     */
    public Product getProduct(String productName) {
        return productMap.get(productName);
    }
}
