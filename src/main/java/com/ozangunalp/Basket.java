/*
 * Copyright (C) by Courtanet, All Rights Reserved.
 */
package com.ozangunalp;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Basket implementation with immutable list of products
 * Can be modified later to enable mutable basket
 */
public class Basket {

    /**
     * Product by count
     */
    private final Map<String, Integer> products;

    public Basket(String... productNames) {
        this.products = Collections.unmodifiableMap(
                Arrays.stream(productNames).collect(Collectors.toMap(p -> p, p -> 1, Integer::sum))
        );
    }

    public Map<String, Integer> getProducts() {
        return products;
    }

    public int getProductCount(String productName) {
        Integer count = products.get(productName);
        return (count != null) ? count : 0;
    }
}
