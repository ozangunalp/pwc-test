/*
 * Copyright (C) by Courtanet, All Rights Reserved.
 */
package com.ozangunalp;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * For the sake of simplicity arithmetic operations are done using double type.
 * This is obviously not a best practice and should be replaced by BigDecimal.
 * Checkout class avoids this issue by only doing the total calculation using BigDecimal.
 */
public class Checkout {

    private static final Logger LOGGER = Logger.getLogger(Checkout.class.getName());

    // init decimal formatters
    private static final DecimalFormat PENNY_FORMAT = new DecimalFormat("00'p'");
    private static final DecimalFormat HASH_TAG_FORMAT;

    static {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setCurrencySymbol("#");
        HASH_TAG_FORMAT = new DecimalFormat("\u00A40.00", symbols);
    }

    private final double subTotal;
    private final List<AppliedOffer> appliedOffers;

    public static Checkout checkout(Instant checkoutTime, Basket basket, ProductCatalog catalog, List<Offer> offers) {
        LOGGER.info("Checkout at " + checkoutTime);
        // TODO Should verify if product is still available
        double subTotal = basket.getProducts().entrySet().stream()
                .mapToDouble(p -> catalog.getProduct(p.getKey()).getPrice() * p.getValue())
                .sum();
        LOGGER.info("Sub total " + subTotal);
        List<AppliedOffer> appliedOffers = offers.stream()
                .filter(o -> o.isApplicable(checkoutTime)) // filter out timed offers
                .map(o -> o.applyTo(basket, catalog))
                .filter(Objects::nonNull) // Filter out non applicable offers
                .collect(Collectors.toList());
        LOGGER.info("Applied " + appliedOffers.size() + " offer(s)");
        return new Checkout(subTotal, appliedOffers);
    }

    public static String renderAmount(double amount) {
        return (amount < 1.0) ? PENNY_FORMAT.format(amount * 100) : HASH_TAG_FORMAT.format(amount);
    }

    public Checkout(double subTotal, List<AppliedOffer> appliedOffers) {
        this.subTotal = subTotal;
        this.appliedOffers = appliedOffers;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public List<AppliedOffer> getAppliedOffers() {
        return appliedOffers;
    }

    public double getDiscount() {
        return appliedOffers.stream().mapToDouble(AppliedOffer::getDiscount).sum();
    }

    public double getTotal() {
        return BigDecimal.valueOf(subTotal).subtract(BigDecimal.valueOf(getDiscount())).doubleValue();
    }

    public String render() {
        return
                "Subtotal: " + renderAmount(getSubTotal()) + "\n"
                        + (appliedOffers.size() > 0 ?
                        appliedOffers.stream().map(o -> o.getOffer().getShortDescription() + ": -" + renderAmount(o.getDiscount()))
                                .collect(Collectors.joining("\n")) : "(no offers available)")
                        + "\n"
                        + "Total: " + renderAmount(getTotal());
    }
}
