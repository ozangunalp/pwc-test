package com.ozangunalp;

import java.time.Instant;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class Offer {

    private static final Logger LOGGER = Logger.getLogger(Offer.class.getName());

    private final String fullDescription;
    private final String shortDescription;
    private final Predicate<Instant> timeApplicability;
    private final BiFunction<Basket, ProductCatalog, Double> offerFunction;

    public Offer(String fullDescription, String shortDescription, Predicate<Instant> timeApplicability,
            BiFunction<Basket, ProductCatalog, Double> offerFunction) {
        this.fullDescription = fullDescription;
        this.shortDescription = shortDescription;
        this.timeApplicability = timeApplicability;
        this.offerFunction = offerFunction;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public boolean isApplicable(Instant time) {
        return timeApplicability.test(time);
    }

    public AppliedOffer applyTo(Basket basket, ProductCatalog productCatalog) {
        Double discount = offerFunction.apply(basket, productCatalog);
        if (discount != 0.0) {
            LOGGER.info("Applied " + fullDescription);
            return new AppliedOffer(this, discount);
        }
        return null;
    }
}
